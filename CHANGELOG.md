# Changelog

## 2.3.5 - 2024-06-12

### Feat

- ajout du réseau social Béhance

## 2.3.2 - 2023-06-07

### Changed

- Compatibilité SPIP 4.2
- ajout d'un CHANGELOG.md et d'un README.md