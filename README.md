# Liens vers les réseaux sociaux

Ce plugin permet de déclarer les comptes des réseaux sociaux associés à votre site et d’en afficher facilement les liens sur le site public, pour que les internautes puissent les suivre.

## Documentation

https://contrib.spip.net/4720