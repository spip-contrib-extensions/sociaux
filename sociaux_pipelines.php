<?php
/**
 * Plugin sociaux
 * Licence GPL3
 *
 */

if (!defined("_ECRIRE_INC_VERSION")) {
	return;
}

/**
 * Inserer une CSS pour le contenu embed
 *
 * @param $head
 *
 * @return string
 */
function sociaux_insert_head_css($head) {
	include_spip('inc/config');
	if (lire_config('sociaux/css', 0)) {
		$head .= '<link rel="stylesheet" type="text/css" href="' . timestamp(find_in_path('lib/socicon/style.css')) . '" />' . "\n";
		$head .= '<link rel="stylesheet" type="text/css" href="' . timestamp(find_in_path('css/sociaux.css')) . '" />' . "\n";
	}

	return $head;
}

function sociaux_sociaux_lister($flux) {
	/**
	 * On reprend les éléments initiaux de la v1 du plugin :
	 * facebook, twitter, instagram, blogger, pinterest, linkedin, youtube, rss, email, tripadvisor, vimeo, flickr
	 *
	 */
	if (isset($flux['data'])) {
		$flux['data']['mail'] = 'E-mail';
		$flux['data']['rss'] = 'RSS';
		$flux['data']['facebook'] = 'Facebook';
		$flux['data']['x'] = 'X';
		$flux['data']['twitter'] = 'Twitter';
		$flux['data']['mastodon'] = 'Mastodon';
		$flux['data']['bluesky'] = 'Bluesky';
		$flux['data']['instagram'] = 'Instagram';
		$flux['data']['threads'] = 'Threads';
		$flux['data']['blogger'] = 'Blogger';
		$flux['data']['pinterest'] = 'Pinterest';
		$flux['data']['linkedin'] = 'Linkedin';
		$flux['data']['youtube'] = 'Youtube';
		$flux['data']['tripadvisor'] = 'TripAdvisor';
		$flux['data']['vimeo'] = 'Vimeo';
		$flux['data']['flickr'] = 'Flickr';
		$flux['data']['viber'] = 'Viber';
		$flux['data']['whatsapp'] = 'Whatsapp';
		$flux['data']['signal'] = 'Signal';
		$flux['data']['skype'] = 'Skype';
		$flux['data']['bandcamp'] = 'Bandcamp';
		$flux['data']['gitlab'] = 'Gitlab';
		$flux['data']['github'] = 'Github';
		$flux['data']['behance'] = 'Behance';
	}

	return $flux;
}

/**
 * Ajoute les scripts JS et CSS de saisies dans l'espace privé
 *
 * @param string $flux
 * @return string
 **/
function sociaux_header_prive($flux){
	$css = timestamp(find_in_path('lib/socicon/style.css'));
	$flux .= "\n<link rel='stylesheet' href='$css' type='text/css' media='all' />\n";
	return $flux;
}

